﻿# Use the Apprenda Powershell module to upload, promote, and remove
# apps thus exercising the platform.  It's not comprehensive but it
# hits a bunch of high points.

# FIXME - might be nice to make app alias root a parameter
param (
    [Parameter(Mandatory=$true)][string]$username,
    
    [Parameter(Mandatory=$true)][Security.SecureString]$SecurePassword,
    
    [Parameter(Mandatory=$true)][string]$url,

    [Parameter(Mandatory=$true)][string]$tenantAlias,

    [Parameter()][String]$archivePath = ".\archive\timecard.zip",

    [Parameter()][String]$logPath = ".\testlog.log",

    [Parameter()][string]$PowerShellModulePath = ".\PowerShell\Apprenda-Powershell",
   
    [Parameter()][int]$delayMinutes = 5,

    [Parameter()][int]$hostCount = 1,

    [Parameter()][bool]$delete = $true,
    
    [Parameter()][switch]$deploy = $true,
    
    [Parameter()][switch]$scale = $true,

    [Parameter()][int]$cycles = 1
)

Write-Debug "`$username = $username"
Write-Debug "`$url = $url"
Write-Debug "`$tenantAlias = $tenantAlias"
Write-Debug "`$archivePath = $archivePath"
Write-Debug "`$logPath = $logPath"
Write-Debug "`$PowershellModulePath = $PowershellModulePath"
Write-Debug "`$hostCount = $hostCount"
Write-Debug "`$deploy = $deploy"
Write-Debug "`$scale = $scale"
# FIXME - add rest of args (except pw, of course)

Write-Host "Checking for Apprenda Powershell Module."
$apprenda = Get-Module Apprenda-Powershell
if ($apprenda -eq $null) { 
    Write-Host "Loading Apprenda Powershell Module."
    Import-Module $PowershellModulePath
} else {
    Write-Host "Apprenda Powershell Module found."
}


$action = {
    $ErrorActionPreference = 'Continue'

    $aliasRoot = "newTest"
    $applicationName = "Time Card Application"
    $applicationDescription = "Time Card Test Payload"
    $versionAlias = "v1"
    $promotionStage = "published"

    # Get the clear text password from the secure string for REST API use
    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
    $password = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)

    Write-Host "Starting Apprenda Session"
    New-ApprendaSession -Username $username `
    			-Password $password `
			-Url $url `
			-Tenant $tenantAlias

    # Now that we don't need the clear text any more, remove it.
    $password = $null

    if ($deploy -and $delete) {
        $ErrorActionPreference = 'Continue'
        Write-Host "Getting the oldest application alias"
        $application = Get-ApprendaApplication | ? { $_.Name -match '[(]\d[)]' } | Sort-Object description | Select-Object -First 1
	if ($($applicatoin.alias) -eq "") {
            Write-Host "No previous applications found."
	}
	else {
            Write-Host "Removing Application $($application.Name)"
	    Try {
                Remove-ApprendaApplication -ApplicationAlias $($application.alias) -TimeoutSec 600		   
            }
	    Catch {
	        Write-Host "Failed."
            }
        }
    }


    if ($deploy) {
        $ErrorActionPreference = 'Continue'
        # Get a new random suffix for the next deployment
        $suffix = [System.IO.Path]::GetRandomFileName() -replace "\.", ""

        $applicationAlias = $($aliasRoot + $suffix)

	$d = $(Get-Date -format "yyyy-MM-dd HH:mm")

	$n = "$applicationName $d ($c)"
	
        Write-Host "Creating the '$n' ($applicationAlias) application"
        New-ApprendaApplication -ApplicationAlias $applicationAlias -ApplicationName "$n" -Description "$applicationDescription at $(Get-Date)"

        Write-Host "Uploading the $archivePath file."
        Set-ApprendaApplicationArchive -ApplicationAlias $applicationAlias -VersionAlias $versionAlias -ArchivePath $archivePath -TimeoutSec 600

        Write-Host "Promoting the $applicationAlias application."
        Promote-ApprendaApplication -ApplicationAlias $applicationAlias -VersionAlias $versionAlias -Stage $promotionStage -TimeoutSec 600
    }
    
    if ($deploy -and $scale) {
        $ErrorActionPreference = 'Continue'
        Write-Host "Setting Min Instance Count to $($hostCount * 2) for the UI."
        $url = "$($ApprendaSession.url)/developer/api/v1/components/$applicationAlias/v1/ui-root?action=SetInstanceCount&minCountEnabled=true&minCount=$($hosts.Count)"
        Invoke-RestMethod -Method Post -Uri $url -Headers $ApprendaSession.headers -TimeoutSec 600
		      
        Write-Host "Setting Min Instance Count to $($hostCount * 4) for the wcf service"
        $url = "$($ApprendaSession.url)/developer/api/v1/components/$applicationAlias/v1/wcfsvc-TimecardService?action=SetInstanceCount&minCountEnabled=true&minCount=$($hosts.Count*4)"
        Invoke-RestMethod -Method Post -Uri $url -Headers $ApprendaSession.headers -TimeoutSec 600
    }
		      
    Write-Host "Closing the Session"
    Close-ApprendaSession -TimeoutSec 600

}


Start-Transcript -Path $logPath -Append

for ($c=0; $c -lt $cycles; $c++) {
    if ($c -ne 0) {
        Write-Host "Pausing for $delayMinutes minutes"
        Start-Sleep -s $($delayMinutes * 60)
    }
    
    Write-Host "=== Cycle $($c+1) of $cycles ==="
    Try {
        Invoke-Command $action
    }
    Catch [Exception] {
	Write-Host "Failed.  Trying again"
        echo $_.Exception | format-list -force
    }
}

Stop-Transcript

