﻿# Use the Apprenda Powershell module to upload, promote, and remove
# apps thus exercising the platform.  It's not comprehensive but it
# hits a bunch of high points.

# FIXME - might be nice to make app alias root a parameter
param (
    [Parameter(Mandatory=$true)][string]$username,
    
    [Parameter(Mandatory=$true)][Security.SecureString]$SecurePassword,
    
    [Parameter(Mandatory=$true)][string]$url,

    [Parameter(Mandatory=$true)][string]$tenantAlias,

    [Parameter()][String]$archivePath = ".\archive\timecard.zip",

    [Parameter()][String]$logPath = ".\testlog.log",

    [Parameter()][string]$PowerShellModulePath = ".\PowerShell\Apprenda-Powershell",
   
    [Parameter()][int]$delayMinutes = 5,

    [Parameter()][int]$hostCount = 1,

    [Parameter()][bool]$delete = $true,
    
    [Parameter()][switch]$deploy = $true,
    
    [Parameter()][switch]$scale = $true,

    [Parameter()][int]$cycles = 1
)

Write-Debug "`$username = $username"
Write-Debug "`$url = $url"
Write-Debug "`$tenantAlias = $tenantAlias"
Write-Debug "`$archivePath = $archivePath"
Write-Debug "`$logPath = $logPath"
Write-Debug "`$PowershellModulePath = $PowershellModulePath"
Write-Debug "`$hostCount = $hostCount"
Write-Debug "`$deploy = $deploy"
Write-Debug "`$scale = $scale"
# FIXME - add rest of args (except pw, of course)


Start-Transcript -Path $logPath -Append

# Get the clear text password from the secure string for REST API use
$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
$password = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)

for ($c=0; $c -lt $cycles; $c++) {
    if ($c -ne 0) {
        Write-Host "Pausing for $delayMinutes minutes"
        Start-Sleep -s $($delayMinutes * 60)
    }
    
    Write-Host "=== Cycle $($c+1) of $cycles ==="
    Try {
        # Invoke-Command $action
	PowerShell -Command ".\worker.ps1" -username $username -password $password -url $url -tenantalias $tenantalias -archivepath $archivepath -powershellmodulepath $powershellmodulepath -hostcount $hostcount -delete:`$$delete -deploy:`$$deploy -scale:`$$scale
    }
    Catch [Exception] {
	Write-Host "Failed.  Trying again"
        echo $_.Exception | format-list -force
    }
}

Stop-Transcript

